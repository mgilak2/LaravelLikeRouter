<?php
include __DIR__ . "/../vendor/autoload.php";
include __DIR__ . "/routes.php";

\LaravelLikeRouter\WorkFlow::init($routeCollector, ['en', 'fr']);