<?php

use LaravelLikeRouter\RoutesCollection;
use LaravelLikeRouter\RouteCollector;

$routeCollector = new RouteCollector(new RoutesCollection());

$routeCollector->addGroupRoute(['route' => "hasan/gilak"], function () use ($routeCollector) {
    $routeCollector->addGetRoute("", ['function' => function () {
        return "yooo hoo , you just utilize route group !";
    }]);

    $routeCollector->addGetRoute("agha", ['function' => function () {
        return "eyval agha eyval !";
    }]);

    $routeCollector->addGroupRoute(['route' => "dadash"], function () use ($routeCollector) {
        $routeCollector->addGetRoute("joon", ['function' => function () {
            return "joonam joon !";
        }]);
    });
});

$routeCollector->addGetRoute("", ['function' => function () {
    return "this is homepage! good luck";
}]);

$routeCollector->addGetRoute("[M]/hasan/gilak/hakim/abadi", ['function' => function () {
    return "we are in hasan/gilak/hakim/abadi";
}]);

$routeCollector->addGetRoute("{username}/profile", ['function' => function ($username) {
    return "I`m inside " . $username . " profile";
}]);

$routeCollector->addPostRoute("{name}/{last}/hakim/abad", ['function' => function ($name, $last) {
    return "I`m inside " . $name . " " . $last . " page";
}]);

$routeCollector->addPostRoute("[M]/elyas/gilak/hakim/abadi", ['function' => function () {
    return "I`m inside elyas/gilak/hakim/abad page";
}]);

///////////////////////////////////////////////////////

$routeCollector->addGetRoute("[M]/{name}/{last}/profile", ['function' => function ($name, $last) {
    var_dump(\LaravelLikeRouter\Environments::currentRoute());
}]);

////////////////////////////////////////////

$routeCollector->addGetRoute('sample', ['controller' => "SampleController@index", "resolver" => function () {
    return new \App\Controller\SampleController();
}]);
