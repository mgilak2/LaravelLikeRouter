<?php namespace LaravelLikeRouter;

class RoutesCollection
{
    use RouteSegmentsTrait;

    const POST_METHOD = "POST";
    const GET_METHOD = "POST";
    protected $allGetRoutes = [];
    protected $allPostRoutes = [];
    protected $allRouteGroup = [];

    /**
     * @return array
     */
    public function getAllGetRoute()
    {
        return $this->allGetRoutes;
    }

    /**
     * @return array
     */
    public function getAllPostRoute()
    {
        return $this->allPostRoutes;
    }

    /**
     * @return array
     */
    public function getAllRoutes()
    {
        return ["POST" => $this->getAllPostRoute(),
            "GET" => $this->getAllGetRoute()];
    }

    /**
     * @param Route $route
     * @param $routeHolderBasedOnRouteMethod
     */
    private function insert(Route $route, &$routeHolderBasedOnRouteMethod)
    {
        $routeAddress = $this->getRouteSegmentsFromRouteAddress($route->getRouteAddress());
        $sanitizedRouteAddress = $this->getSanitizedRoute($routeAddress);
        $routeHolderBasedOnRouteMethod[$sanitizedRouteAddress] = $route;
    }

    /**
     * @param Route $routeObjectData
     */
    public function insertGetMethodRoute(Route $routeObjectData)
    {
        $this->insert($routeObjectData, $this->allGetRoutes);
    }

    /**
     * @param Route $routeObjectData
     */
    public function insertPostMethodRoute(Route $routeObjectData)
    {
        $this->insert($routeObjectData, $this->allPostRoutes);
    }
}

