<?php namespace LaravelLikeRouter;

trait RouteSegmentsTrait
{
    /**
     * @param Route $route
     * @return array
     */
    public function splitRouteIntoSegments(Route $route)
    {
        $routeSegments = $this->removeEmptyRouteSegments(explode("/", $route->getRouteAddress()));
        if (count($routeSegments) == 0)
            return [""];
        return $routeSegments;
    }

    /**
     * @param $routeAddress
     * @return array
     */
    public function getRouteSegmentsFromRouteAddress($routeAddress)
    {
        $routeSegments = $this->removeEmptyRouteSegments(explode("/", $routeAddress));
        if (count($routeSegments) == 0)
            return [""];
        return $routeSegments;
    }

    /**
     * @param $routeSegments
     * @return array
     */
    private function removeEmptyRouteSegments($routeSegments)
    {
        $sanitizedSegments = [];
        for ($i = 0; $i < count($routeSegments); $i++)
            if (trim($routeSegments[$i]) != "")
                $sanitizedSegments[] = $routeSegments[$i];
        return $sanitizedSegments;
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     * @return bool
     */
    public function hasSameAmountOfSegmentsAsRequestedRouteHas(Route $route, Route $requestedRoute)
    {
        $routeSegments = $this->getRouteSegmentsFromRouteAddress($route->getFilledRoute());
        $requestedRouteSegments = $this->getRouteSegmentsFromRouteAddress($requestedRoute->getRouteAddress());
        return (count($routeSegments) == count($requestedRouteSegments))
            ? true : false;
    }

    /**
     * @param array $segments
     * @return string
     */
    public function getSanitizedRoute(array $segments)
    {
        return join("/", $segments);
    }
}