<?php namespace LaravelLikeRouter;

class RouteGroup
{
    protected $attributes;

    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    public function executeRouteGroupCallback()
    {
        call_user_func_array($this->attributes['callback'], []);
    }
}