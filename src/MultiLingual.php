<?php namespace LaravelLikeRouter;

class MultiLingual
{
    protected $supportedLanguages = [];
    protected $languageWildCardIndex = 0;
    protected $currentLanguage;

    /**
     * @param array $languages
     */
    public function supportedLanguages(array $languages = [])
    {
        $this->supportedLanguages = $languages;
    }

    /**
     * @param $index
     */
    public function setLanguageWildCardIndex($index)
    {
        $this->languageWildCardIndex = $index;
    }

    /**
     * @return int
     */
    public function getLanguageWildCardIndex()
    {
        return $this->languageWildCardIndex;
    }

    /**
     * @param array $segments
     * @return bool
     */
    public function doesApplyMultiLingual(array $segments)
    {
        if (in_array($segments[$this->languageWildCardIndex], $this->supportedLanguages))
            return true;
        return false;
    }

    /**
     * @param array $segments
     * @return mixed
     */
    public function getLanguage(array $segments)
    {
        return $segments[$this->languageWildCardIndex];
    }

    /**
     * @param $routeAddress
     * @return bool
     */
    public function doesRouteAddressContainMultiLingualWildCard($routeAddress)
    {
        preg_match("/\[M\]/", $routeAddress, $output_array);
        if (isset($output_array[0]))
            return true;
        return false;
    }

    /**
     * @param Route $route
     * @return bool
     */
    public function doesGivenRouteContainMultiLingualWildCard(Route $route)
    {
        return $this->doesRouteAddressContainMultiLingualWildCard($route->getRouteAddress());
    }

    /**
     * @param array $segments
     * @return bool
     */
    public function doesGivenRouteContainMultiLingualAbbreviation(array $segments)
    {
        return $this->doesApplyMultiLingual($segments);
    }

    /**
     * @param Route $route
     * @param $language
     * @return mixed
     */
    public function replaceRouteMultiLingualWildCard(Route $route, $language)
    {
        return preg_replace("/\[M\]/", $language, $route->getRouteAddress());
    }

    /**
     * @param Route $route
     * @return mixed
     */
    public function discardMultiLingualWildCard(Route $route)
    {
        return preg_replace("/\[M\]/", "", $route->getRouteAddress());
    }
}