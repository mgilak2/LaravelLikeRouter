<?php namespace LaravelLikeRouter;

use Exception;
use LaravelLikeRouter\Http\RequestedRouteHandler;
use LaravelLikeRouter\Http\ResponseHandler;

class WorkFlow
{
    /**
     * @var RouteCollector
     */
    protected $routeCollector;
    /**
     * @var FindRequestedRoute
     */
    protected $finder;
    /**
     * @var ResponseHandler
     */
    protected $response;
    /**
     * @var ExecuteFunction
     */
    private $executor;

    public function __construct(RouteCollector $collector,
                                FindRequestedRoute $finder,
                                ResponseHandler $response,
                                ExecuteFunction $executor)
    {
        $this->routeCollector = $collector;
        $this->finder = $finder;
        $this->response = $response;
        $this->executor = $executor;
    }

    /**
     * @param Route $requestedRoute
     * @throws Exception
     */
    public function answerTheRequest(Route $requestedRoute)
    {
        $route = $this->findRouteBasedOnItsMethod($requestedRoute);
        if (is_string($route))
            throw new Exception($route);
        else {
            if (isset($route->getAttributes()['function']))
                $this->response->response($this->executor->executeCallback($route))->send();
            else
                $this->response->response($this->executor->executeController($route))->send();
        }
    }

    /**
     * @param $route
     * @return Route|string
     */
    private function findRouteBasedOnItsMethod($route)
    {
        return $this->finder
            ->inside($this->routeCollector->getCollectedRoutes())
            ->findRoute($route);
    }

    public static function init($routeCollector, $languages = [])
    {
        $multiLingual = new MultiLingual();
        $multiLingual->supportedLanguages($languages);
        $multiLingual->setLanguageWildCardIndex(0);
        $matchRoute = new MatchRoutes(new WildCards($multiLingual));
        $finder = new FindRequestedRoute($matchRoute);
        $workflow = new WorkFlow($routeCollector, $finder, new ResponseHandler(), new ExecuteFunction());
        $requestedRoute = new RequestedRouteHandler();
        $routeDataObjectFromRequestedRoute = $requestedRoute->getRouteDataObject();
        try {
            $workflow->answerTheRequest($routeDataObjectFromRequestedRoute);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}