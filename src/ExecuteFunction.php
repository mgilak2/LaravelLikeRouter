<?php namespace LaravelLikeRouter;

class ExecuteFunction
{
    /**
     * @param Route $route
     * @return mixed
     */
    public function executeCallback(Route $route)
    {
        if ($route->hasWildCards())
            return call_user_func_array($route->getAttributeItem("function"),
                $route->getWildCardAsArguments());
        else
            return call_user_func($route->getAttributeItem("function"));
    }

    /**
     * @param Route $route
     * @return mixed
     */
    public function executeController(Route $route)
    {
        if ($route->hasWildCards())
            return call_user_func_array([call_user_func($route->getAttributeItem('resolver'))
                , $route->getControllerFunction()],
                $route->getWildCardAsArguments());
        else
            return call_user_func([call_user_func($route->getAttributeItem('resolver')),
                $route->getControllerFunction()]);
    }
}