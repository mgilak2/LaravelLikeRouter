<?php namespace LaravelLikeRouter;

class Route
{
    protected $routeAddress;
    protected $filledRoute;
    protected $attributes;
    protected $method;
    protected $parent;
    protected $wildCards = [];
    protected $arguments;
    protected $language = null;
    protected $options = [
        "function" => null,
        "before" => null,
        "after" => null,
        "as" => null,
        "controller" => [null, null]
    ];
    protected $beforeFilters = [];
    protected $afterFilters = [];

    public function __construct($route = "", $attributes = [])
    {
        $this->routeAddress = $route;
        $this->attributes = array_merge($this->options, $attributes);
    }

    /**
     * @return string
     */
    public function getFilledRoute()
    {
        return $this->filledRoute;
    }

    /**
     * @param $filledRoute
     */
    public function setFilledRoute($filledRoute)
    {
        $this->filledRoute = $filledRoute;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getAttributeItem($key)
    {
        return $this->attributes[$key];
    }


    public function getControllerFunction()
    {
        return explode("@",$this->attributes["controller"])[1];
    }

    /**
     * @param $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getRouteAddress()
    {
        return $this->routeAddress;
    }

    /**
     * @param $routeAddress
     */
    public function setRouteAddress($routeAddress)
    {
        $this->routeAddress = $routeAddress;
    }

    /**
     * @return array
     */
    public function getWildCardAsArguments()
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     */
    public function setWildCardAsArguments(array $arguments)
    {
        $this->arguments = $arguments;
    }

    /**
     * @return bool
     */
    public function hasWildCards()
    {
        return (count($this->arguments) > 0) ? true : false;
    }

    /**
     * @param $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return null|string
     */
    public function getLanguage()
    {
        return $this->language;
    }
}