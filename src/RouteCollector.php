<?php namespace LaravelLikeRouter;

use Closure;

class RouteCollector
{
    /**
     * @var RoutesCollection
     */
    protected $collection;
    protected $baseRoute = '';
    protected $baseNamespace = '';
    protected $routeGroupStack = [];

    public function __construct(RoutesCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return array
     */
    public function getCollectedRoutes()
    {
        return $this->collection->getAllRoutes();
    }

    /**
     * @param $route
     * @param $attributes
     * @return Route
     */
    private function createRouteDataObject($route, $attributes)
    {
        $routeDataObject = new Route($route, $attributes);
        $routeDataObject->setRouteAddress($this->baseRoute . $routeDataObject->getRouteAddress());
        return $routeDataObject;
    }

    /**
     * @param $route
     * @param $attributes
     */
    public function addGetRoute($route, $attributes)
    {
        $routeDataObject = $this->createRouteDataObject($route, $attributes);
        $this->collection->insertGetMethodRoute($routeDataObject);
    }

    /**
     * @param $route
     * @param $attributes
     */
    public function addPostRoute($route, $attributes)
    {
        $routeDataObject = $this->createRouteDataObject($route, $attributes);
        $this->collection->insertPostMethodRoute($routeDataObject);
    }

    /**
     * @param $attributes
     */
    private function pushRouteGroupStack($attributes)
    {
        $this->routeGroupStack[] = $attributes;
        $this->baseRoute = $this->getBaseRouteOutOfRouteGroupStack();
    }

    private function popRouteGroupStack()
    {
        array_pop($this->routeGroupStack);
        $this->baseRoute = $this->getBaseRouteOutOfRouteGroupStack();
    }

    /**
     * @return string
     */
    private function getBaseRouteOutOfRouteGroupStack()
    {
        $baseRoute = "";
        foreach ($this->routeGroupStack as $attributes)
            if (isset($attributes['route']))
                $baseRoute .= "/" . $attributes['route'] . '/';
        return $baseRoute;
    }

    /**
     * @param $attributes
     */
    private function callNestedRouteGroupCallbacks($attributes)
    {
        $this->pushRouteGroupStack($attributes);
        (new RouteGroup($attributes))->executeRouteGroupCallback();
        $this->popRouteGroupStack();
    }

    /**
     * @param $attributes
     * @param Closure $callback
     */
    public function addGroupRoute($attributes, Closure $callback)
    {
        $attributes['callback'] = $callback;
        $this->callNestedRouteGroupCallbacks($attributes);
    }
}