<?php namespace LaravelLikeRouter;

class MatchRoutes
{
    use RouteSegmentsTrait;

    /**
     * @var WildCards
     */
    private $wildCardHandler;

    /**
     * MatchRoutes constructor.
     * @param WildCards $wildCardHandler
     */
    public function __construct(WildCards $wildCardHandler)
    {
        $this->wildCardHandler = $wildCardHandler;
    }

    /**
     * @return WildCards
     */
    public function getWildCardHandler()
    {
        return $this->wildCardHandler;
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     * @return bool
     */
    private function isAnySimpleRouteIsEqualTo(Route $route, Route $requestedRoute)
    {
        $this->wildCardHandler->considerMultilingual($route, $requestedRoute);
        return $this->getSanitizedRoute($this->getRouteSegmentsFromRouteAddress($route->getFilledRoute()))
        == $this->getSanitizedRoute($this->getRouteSegmentsFromRouteAddress($requestedRoute->getRouteAddress()));
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     * @return bool
     */
    public function isEqualToRequestedRoute(Route $route, Route $requestedRoute)
    {
        if ($this->wildCardHandler->isAnyWildCardRouteEqualTo($route, $requestedRoute)
            || $this->isAnySimpleRouteIsEqualTo($route, $requestedRoute)
        )
            return true;
        return false;
    }
}