<?php namespace LaravelLikeRouter\Http;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ExtendedSymfonyHttpResponse extends Response
{
    /**
     * @param string $content
     * @param int $status
     * @param array $headers
     * @return ExtendedSymfonyHttpResponse
     */
    public static function create($content = '', $status = 200, $headers = array())
    {
        return new ExtendedSymfonyHttpResponse($content, $status, $headers);
    }

    /**
     * @param $data
     * @return $this
     */
    public function json($data)
    {
        $this->headers = new ResponseHeaderBag(["Content-Type" => "application/json"]);
        $this->setContent(json_encode($data));
        return $this;
    }
}