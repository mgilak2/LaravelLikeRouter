<?php namespace LaravelLikeRouter\Http;

use LaravelLikeRouter\Route;
use LaravelLikeRouter\RouteSegmentsTrait;
use Symfony\Component\HttpFoundation\Request;

class RequestedRouteHandler
{
    use RouteSegmentsTrait;

    /**
     * @var Route
     */
    protected $routeDataObject = null;
    /**
     * $var Request
     */
    protected $request = null;

    /**
     * @return null|Request
     */
    private function request()
    {
        if (is_null($this->request))
            $this->request = Request::createFromGlobals();
        return $this->request;
    }

    /**
     * @return string
     */
    private function getRouteAddressOutOfRequest()
    {
        return $this->request()->getPathInfo();
    }

    /**
     * @return Route
     */
    private function createRoute()
    {
        $route = new Route($this->getRouteAddressOutOfRequest(), []);
        $this->setRouteMethodBasedOnRequestedMethod($route);
        return $route;
    }

    /**
     * @return bool
     */
    private function isPost()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
            return true;
        return false;
    }

    /**
     * @return bool
     */
    private function isGet()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
            return true;
        return false;
    }

    /**
     * @param Route $route
     */
    public function setRouteMethodBasedOnRequestedMethod(Route $route)
    {
        if ($this->isPost())
            $route->setMethod("POST");
        elseif ($this->isGet())
            $route->setMethod("GET");
    }

    /**
     * @return Route
     */
    public function getRouteDataObject()
    {
        if (is_null($this->routeDataObject))
            $this->routeDataObject = $this->createRoute();
        return $this->routeDataObject;
    }
}