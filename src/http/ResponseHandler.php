<?php namespace LaravelLikeRouter\Http;

class ResponseHandler
{
    protected $responseHandler;

    /**
     * @param string $content
     * @param int $status
     * @param array $headers
     * @return ExtendedSymfonyHttpResponse
     */
    public function response($content = '', $status = 200, $headers = array())
    {
        if (is_null($this->responseHandler))
            $this->responseHandler = ExtendedSymfonyHttpResponse::create($content, $status, $headers);
        return $this->responseHandler;
    }
}