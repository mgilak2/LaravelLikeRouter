<?php namespace LaravelLikeRouter;

class FindRequestedRoute
{
    protected $allRoutes;
    protected $foundedRoute = null;
    /**
     * @var MatchRoutes
     */
    private $match;

    public function __construct(MatchRoutes $match)
    {
        $this->match = $match;
    }

    /**
     * @param array $allRoutes
     * @return $this
     */
    public function inside(array $allRoutes)
    {
        $this->allRoutes = $allRoutes;
        return $this;
    }

    /**
     * @param string $message
     * @return string
     */
    private function noMatch($message = "")
    {
        return 'there is no such route : ' . $message;
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     */
    private function mergeRoutes(Route $route, Route $requestedRoute)
    {
        $this->foundedRoute = $route;
        $wildCardAsArguments = $this->match->getWildCardHandler()
            ->getWildCardAsArgumentsFromRequestedRoute($requestedRoute);

        $this->foundedRoute->setWildCardAsArguments($wildCardAsArguments);
    }

    /**
     * @param Route $requestedRoute
     * @return bool
     * @internal param $type
     * @internal param Route $route
     */
    private function isThisRouteExists(Route $requestedRoute)
    {
        foreach ($this->allRoutes[$requestedRoute->getMethod()] as $route)
            if ($this->match->isEqualToRequestedRoute($route, $requestedRoute)) {
                $this->mergeRoutes($route, $requestedRoute);
                $this->setEnvironment($requestedRoute);
                return true;
            }
        return false;
    }

    /**
     * @param Route $requestedRoute
     */
    private function setEnvironment(Route $requestedRoute)
    {
        Environments::$currentRoute = $this->foundedRoute;
        Environments::$requestedRoute = $requestedRoute;
    }

    /**
     * @param Route $requestedRoute
     * @return Route|string
     */
    public function findRoute(Route $requestedRoute)
    {
        if ($this->isThisRouteExists($requestedRoute))
            return $this->foundedRoute;
        return $this->noMatch($requestedRoute->getMethod());
    }
}
