<?php namespace LaravelLikeRouter;

class WildCards
{
    use RouteSegmentsTrait;

    protected $wildCards = [];
    /**
     * @var
     */
    private $multiLingual;

    /**
     * WildCards constructor.
     * @param MultiLingual $multiLingual
     */
    public function __construct(MultiLingual $multiLingual)
    {
        $this->multiLingual = $multiLingual;
    }

    /**
     * @return array
     */
    public function getWildCards()
    {
        return $this->wildCards;
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     */
    public function considerMultilingual(Route $route, Route $requestedRoute)
    {
        $segments = $this->getRouteSegmentsFromRouteAddress($requestedRoute->getRouteAddress());
        if ($this->multiLingual->doesGivenRouteContainMultiLingualAbbreviation($segments))
            $this->setLanguageFromRequestedRoute($route, $requestedRoute);
        else
            $this->removeLanguageWildCard($route);
    }

    /**
     * @param $segment
     * @return bool
     */
    private function isContainWildCard($segment)
    {
        preg_match("/\{[a-zA-Z]+\}/", $segment, $output_array);
        if (isset($output_array[0]))
            return true;
        return false;
    }

    /**
     * @param array $segments
     * @return array
     */
    public function collectWildCardsOutOfSegments(array $segments)
    {
        $this->wildCards = [];
        foreach ($segments as $index => $segment)
            if ($this->isContainWildCard($segment))
                $this->wildCards[] = ["index" => $index, "segment" => $segment];
        return $this->wildCards;
    }

    /**
     * @param array $requestedRouteSegments
     * @param array $routeSegments
     * @return array
     */
    public function fillWildCards(array $requestedRouteSegments, array $routeSegments)
    {
        foreach ($this->wildCards as $wildCard)
            $routeSegments[$wildCard['index']] = $requestedRouteSegments[$wildCard['index']];
        return $routeSegments;
    }

    /**
     * @param Route $route
     * @return bool
     */
    public function hasWildeCard(Route $route)
    {
        $routeSegments = $this->getRouteSegmentsFromRouteAddress($route->getRouteAddress());
        $this->wildCards = $this->collectWildCardsOutOfSegments($routeSegments);
        return (count($this->wildCards) > 0) ? true : false;
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     * @return bool
     */
    public function isAnyWildCardRouteEqualTo(Route $route, Route $requestedRoute)
    {
        $this->considerMultilingual($route, $requestedRoute);
        if ($this->hasWildeCard($route)
            && $this->hasSameAmountOfSegmentsAsRequestedRouteHas($route, $requestedRoute)
            && $this->isWildCardRouteEqualToRequestedRoute($route, $requestedRoute)
        ) {
            $route->setWildCardAsArguments($this->wildCards);
            return true;
        }
        return false;
    }

    /**
     * @param Route $requestedRoute
     * @return array
     */
    public function getWildCardAsArgumentsFromRequestedRoute(Route $requestedRoute)
    {
        $arguments = [];
        $requestedRouteSegments = $this->getRouteSegmentsFromRouteAddress($requestedRoute->getRouteAddress());
        foreach ($this->wildCards as $wildCard)
            $arguments[] = $requestedRouteSegments[$wildCard['index']];;
        return $arguments;
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     * @return bool
     */
    private function isWildCardRouteEqualToRequestedRoute(Route $route, Route $requestedRoute)
    {
        list($filledRoute, $requestedRoute) = $this->processRoutes($route, $requestedRoute);
        return ($filledRoute == $requestedRoute) ? true : false;
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     */
    private function setLanguageFromRequestedRoute(Route $route, Route $requestedRoute)
    {
        $language = $this->getLanguage($requestedRoute);
        $filledWithLanguage = $this->multiLingual->replaceRouteMultiLingualWildCard($route, $language);
        $route->setFilledRoute($filledWithLanguage);
        $route->setLanguage($language);
    }

    /**
     * @param Route $route
     */
    private function removeLanguageWildCard(Route $route)
    {
        $removedLanguageWildCardRoute = $this->multiLingual->discardMultiLingualWildCard($route);
        $route->setFilledRoute($removedLanguageWildCardRoute);
    }

    /**
     * @param Route $requestedRoute
     * @return mixed
     */
    private function getLanguage(Route $requestedRoute)
    {
        $segments = $this->getRouteSegmentsFromRouteAddress($requestedRoute->getRouteAddress());
        $language = $this->multiLingual->getLanguage($segments);
        return $language;
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     * @return array
     */
    private function fillWildCardsWithRequestedRoute(Route $route, Route $requestedRoute)
    {
        $requestedRouteSegments = $this->getRouteSegmentsFromRouteAddress($requestedRoute->getRouteAddress());
        $routeSegments = $this->getRouteSegmentsFromRouteAddress($route->getFilledRoute());
        $this->renewWildCards($routeSegments);
        $filledWildCardsWithRequestedRoute = $this->fillWildCards($requestedRouteSegments, $routeSegments);
        return $filledWildCardsWithRequestedRoute;
    }

    /**
     * @param Route $route
     * @param Route $requestedRoute
     * @return array
     */
    private function processRoutes(Route $route, Route $requestedRoute)
    {
        $filledWildCardsWithRequestedRoute = $this->fillWildCardsWithRequestedRoute($route, $requestedRoute);
        $filledRoute = $this->getSanitizedRoute($filledWildCardsWithRequestedRoute);
        $requestedRouteSegments = $this->getRouteSegmentsFromRouteAddress($requestedRoute->getRouteAddress());
        $requestedRoute = $this->getSanitizedRoute($requestedRouteSegments);
        return array($filledRoute, $requestedRoute);
    }

    /**
     * @param $routeSegments
     */
    private function renewWildCards($routeSegments)
    {
        $this->wildCards = $this->collectWildCardsOutOfSegments($routeSegments);
    }
}