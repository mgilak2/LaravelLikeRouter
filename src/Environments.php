<?php namespace LaravelLikeRouter;

class Environments
{
    /**
     * @var Route
     */
    public static $requestedRoute;
    /**
     * @var Route
     */
    public static $currentRoute;

    /**
     * @return null|string
     */
    public static function language()
    {
        return self::$currentRoute->getLanguage();
    }

    /**
     * @return Route
     */
    public static function currentRoute()
    {
        return self::$currentRoute;
    }

    /**
     * @return Route
     */
    public static function requestedRoute()
    {
        return self::$requestedRoute;
    }
}