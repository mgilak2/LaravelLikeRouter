<?php

use LaravelLikeRouter\MultiLingual;
use LaravelLikeRouter\Route;

class MultiLingualTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var MultiLingual
     */
    protected $multilingual;

    public function setUp()
    {
        $this->multilingual = new MultiLingual();
    }

    public function testDoesApplyMultiLingual()
    {
        $this->multilingual->supportedLanguages(['fr', 'er']);
        $this->assertTrue($this->multilingual->doesApplyMultiLingual(['fr', 'hasan', 'gilak']));
    }

    public function testDoesRouteAddressContainMultiLingualWildCard()
    {
        $this->assertTrue($this->multilingual->doesRouteAddressContainMultiLingualWildCard("/[M]/hasan/gilak"));
    }

    public function testDoesGivenRouteContainMultiLingualWildCard()
    {
        $route = Mockery::mock(Route::class);
        $route->shouldReceive("getRouteAddress")->once()->andReturn("[M]/hasan/gilak");
        $this->assertTrue($this->multilingual->doesGivenRouteContainMultiLingualWildCard($route));
    }

    public function testDoesGivenRouteContainMultiLingualAbbreviation()
    {
        $this->multilingual->supportedLanguages(['fr', 'en']);
        $this->assertTrue($this->multilingual->doesGivenRouteContainMultiLingualAbbreviation(['fr', 'en']));
    }

    public function testReplaceRouteMultiLingualWildCard()
    {
        $route = Mockery::mock(Route::class);
        $route->shouldReceive('getRouteAddress')->once()->andReturn("[M]/hasan/gilak");
        $this->assertEquals($this->multilingual->replaceRouteMultiLingualWildCard($route, "fr"), "fr/hasan/gilak");
    }

    public function testDiscardMultiLingualWildCard()
    {
        $route = Mockery::mock(Route::class);
        $route->shouldReceive('getRouteAddress')->once()->andReturn("[M]/hasan/gilak");
        $this->assertEquals($this->multilingual->discardMultiLingualWildCard($route, "fr"), "/hasan/gilak");

    }
}