<?php

use LaravelLikeRouter\ExecuteFunction;
use LaravelLikeRouter\Route;

class ExecuteFunctionTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ExecuteFunction
     */
    protected $executeFunction;

    public function setUp()
    {
        $this->executeFunction = new ExecuteFunction();
    }

    public function testExecuteCallbackWithWildCards()
    {
        $route = Mockery::mock(Route::class);
        $route->shouldReceive("hasWildCards")->once()->andReturn(true);
        $route->shouldReceive("getWildCardAsArguments")->once()->andReturn(['hasan', 'gilak']);
        $route->shouldReceive("getAttributeItem")->with("function")->once()->andReturn(function ($name, $last) {
            return "my name is " . $name . " " . $last;
        });
        $this->assertEquals($this->executeFunction->executeCallback($route), "my name is hasan gilak");
        $route->mockery_verify();
    }

    public function testExecuteCallbackWithSimpleRoute()
    {
        $route = Mockery::mock(Route::class);
        $route->shouldReceive("hasWildCards")->once()->andReturn(false);
        $route->shouldReceive("getAttributeItem")->with("function")->once()->andReturn(function () {
            return "simple route";
        });

        $this->assertEquals($this->executeFunction->executeCallback($route),
            "simple route");
        $route->mockery_verify();
    }

    public function testExecuteControllerWithWildCards()
    {
        $route = Mockery::mock(Route::class);
        $route->shouldReceive("hasWildCards")->once()->andReturn(true);
        $route->shouldReceive("getWildCardAsArguments")->once()->andReturn(['hasan', 'gilak']);
        $route->shouldReceive("getControllerFunction")->once()->andReturn("index");
        $route->shouldReceive("getAttributeItem")->with("resolver")->once()->andReturn(function () {
            $sample = Mockery::mock("SampleClass");
            $sample->shouldReceive('index')->with('hasan', 'gilak')->once()->andReturn("hello dear");
            return $sample;
        });
        $this->assertEquals($this->executeFunction->executeController($route),
            "hello dear");
        $route->mockery_verify();
    }

    public function testExecuteControllerWithSimpleRoute()
    {
        $route = Mockery::mock(Route::class);
        $route->shouldReceive("hasWildCards")->once()->andReturn(false);
        $route->shouldReceive("getControllerFunction")->once()->andReturn("index");
        $route->shouldReceive("getAttributeItem")->with("resolver")->once()->andReturn(function () {
            $sample = Mockery::mock("SampleClass");
            $sample->shouldReceive('index')->once()->andReturn("hello dear");
            return $sample;
        });
        $this->assertEquals($this->executeFunction->executeController($route),
            "hello dear");
        $route->mockery_verify();
    }
}