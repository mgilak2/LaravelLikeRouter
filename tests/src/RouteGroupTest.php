<?php

use LaravelLikeRouter\RouteGroup;

class RouteGroupTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var RouteGroup
     */
    protected $routeGroup;
    protected $called = false;

    public function setUp()
    {
        $this->routeGroup = new RouteGroup(['callback' => function () {
            $this->called = true;
        }]);
    }

    public function testExecuteRouteGroupCallback()
    {
        $this->routeGroup->executeRouteGroupCallback();
        $this->assertTrue($this->called, 'Callback should be called');
    }
}