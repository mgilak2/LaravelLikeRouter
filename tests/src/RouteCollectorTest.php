<?php


use LaravelLikeRouter\Route;
use LaravelLikeRouter\RouteCollector;
use LaravelLikeRouter\RoutesCollection;

class RouteCollectorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockRouteCollection;
    /**
     * @var RouteCollector
     */
    protected $routeCollector;

    public function setUp()
    {
        $this->mockRouteCollection = Mockery::mock(RoutesCollection::class);
        $this->routeCollector = new RouteCollector($this->mockRouteCollection);
    }

    public function testAddGetRoute()
    {
        $this->mockRouteCollection
            ->shouldReceive("insertGetMethodRoute")->once()->andReturnUsing(function (Route $argument) {
                $this->assertEquals($argument->getRouteAddress(), "hasan/gilak/hakim");
            });

        $this->routeCollector->addGetRoute("hasan/gilak/hakim", ['function' => function () {
            return "hello world!";
        }]);

        $this->mockRouteCollection->mockery_verify();
    }

    public function testAddPostRoute()
    {
        $this->mockRouteCollection
            ->shouldReceive("insertPostMethodRoute")->once()->andReturnUsing(function (Route $argument) {
                $this->assertEquals($argument->getRouteAddress(), "hasan/gilak/hakim");
            });

        $this->routeCollector->addPostRoute("hasan/gilak/hakim", ['function' => function () {
            return "hello world!";
        }]);

        $this->mockRouteCollection->mockery_verify();
    }

    public function testAddGroupRoute()
    {
        $this->mockRouteCollection
            ->shouldReceive("insertGetMethodRoute")->twice()->andReturnUsing(function (Route $argument) {
                $this->assertEquals($argument->getRouteAddress(), "/agha/hasan");
            },function (Route $argument) {
                $this->assertEquals($argument->getRouteAddress(), "/agha//gilak/hasan");
            });
        $this->routeCollector->addGroupRoute(['route' => "agha"], function () {
            $this->routeCollector->addGetRoute('hasan', []);

            $this->routeCollector->addGroupRoute(['route' => "gilak"], function () {
                $this->routeCollector->addGetRoute('hasan', []);
            });
        });
        $this->mockRouteCollection->mockery_verify();
    }
}