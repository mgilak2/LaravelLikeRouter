<?php

use LaravelLikeRouter\MatchRoutes;
use LaravelLikeRouter\Route;
use LaravelLikeRouter\WildCards;

class MatchRoutesTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockWildCards;
    /**
     * @var MatchRoutes
     */
    protected $matchRoutes;

    public function setUp()
    {
        $this->mockWildCards = Mockery::mock(WildCards::class);
        $this->matchRoutes = new MatchRoutes($this->mockWildCards);
    }

    public function testIsEqualToRequestedRoute()
    {
        $route = Mockery::mock(Route::class);
        $route->shouldReceive("getFilledRoute")
            ->once()->andReturn("hasan/gilak/hakim/abadi");

        $requestedRoute = Mockery::mock(Route::class);
        $requestedRoute->shouldReceive("getRouteAddress")
            ->once()->andReturn("hasan/gilak/hakim/abadi");

        $this->mockWildCards->shouldReceive("isAnyWildCardRouteEqualTo")
            ->with($route, $requestedRoute)->once();
        $this->mockWildCards->shouldReceive("considerMultilingual")
            ->with($route, $requestedRoute)->once();

        $this->assertTrue($this->matchRoutes->isEqualToRequestedRoute($route, $requestedRoute));

        $route->mockery_verify();
        $this->mockWildCards->mockery_verify();
        $requestedRoute->mockery_verify();
    }
}