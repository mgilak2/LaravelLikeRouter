<?php
use LaravelLikeRouter\FindRequestedRoute;
use LaravelLikeRouter\MatchRoutes;
use LaravelLikeRouter\MultiLingual;
use LaravelLikeRouter\Route;
use LaravelLikeRouter\WildCards;

class FindRequestedRouteTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var FindRequestedRoute
     */
    protected $findRequestRoute;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockMultiLingual;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockWildCards;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockMatch;

    public function setUp()
    {
        $this->mockMultiLingual = Mockery::mock(MultiLingual::class);
        $this->mockWildCards = Mockery::mock(WildCards::class, [$this->mockMultiLingual]);
        $this->mockMatch = Mockery::mock(MatchRoutes::class, [$this->mockWildCards]);
        $this->findRequestRoute = new FindRequestedRoute($this->mockMatch);
    }

    public function testFindRoute()
    {
        $requestedRoute = Mockery::mock(Route::class);
        $requestedRoute->shouldReceive("getMethod")
            ->once()->andReturn("GET");

        $foundedRoute = Mockery::mock(Route::class);
        $foundedRoute->shouldReceive("setWildCardAsArguments")
            ->with(['hasan', 'gilak'])->once();

        $this->mockMatch->shouldReceive("isEqualToRequestedRoute")
            ->once($foundedRoute, $requestedRoute)->andReturn(true);
        $this->mockMatch->shouldReceive("getWildCardHandler")
            ->once()->andReturn($this->mockWildCards);

        $this->mockWildCards
            ->shouldReceive("getWildCardAsArgumentsFromRequestedRoute")
            ->with($requestedRoute)->once()->andReturn(['hasan', 'gilak']);

        $this->assertEquals($this->findRequestRoute
            ->inside(['GET' => [$foundedRoute]])
            ->findRoute($requestedRoute), $foundedRoute);

        $foundedRoute->mockery_verify();
        $requestedRoute->mockery_verify();
        $this->mockWildCards->mockery_verify();
        $this->mockMatch->mockery_verify();
    }
}